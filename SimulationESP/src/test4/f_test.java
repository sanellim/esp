package test4;
import physics.DerivableFunctionScalar;


public class f_test implements DerivableFunctionScalar {
	@Override
	public double f(double x, double y) {
		return -1*Math.cos(x);
	}

}
