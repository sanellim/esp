package test4;
import physics.DerivableFunctionScalar;


public class exactf implements DerivableFunctionScalar{

	@Override
	public double f(double x, double y) {
		return -1*Math.sin(x);
	}

	
}
