package test1;
import physics.DerivableFunctionScalar;


public class f_test implements DerivableFunctionScalar {
	@Override
	public double f(double x, double y) {
		return 26*Math.cos(x)-5*y;
	}

}
