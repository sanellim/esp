package main;
import general.Entity;
import general.Vector2D;
import general.World2D;
import general.World3D;

import org.jfree.ui.RefineryUtilities;

import physics.DerivableFunctionScalar;
import physics.DerivableFunctionVector;
import charter.DotChart;
import charter.LineChart;
import charter.myXYSeries;


public class Main {

	private static boolean debug = false;
	private static DerivableFunctionScalar f;
	private static DerivableFunctionVector g;
	private static DerivableFunctionScalar exactf;
	private static Entity player;
	
	private static Vector2D initSol;
	
    public static void main(String[] args) {
    	float h = 0;
    	int n = 0;
    	int select = -1;
    	double tollerance = 0;
    	if (args.length == 4) {
    		h = Float.parseFloat(args[0]);
    		n = Integer.parseInt(args[1]);
    		tollerance = Double.parseDouble(args[2]);
    		select = Integer.parseInt(args[3]);
    	}
    	select_test(select);
    	System.out.println("Test " + select + " started with h = " + h + " Cycle = " + n + " Tollerance = " + tollerance);
    	
    	if (select < 10) normalSetup(h, n, tollerance, select);
    	else gravitySetup(h,n,tollerance,select);
    		
    }
    
    public static void gravitySetup(float h, int n, double tollerance, int select) {
    	DotChart chart = new DotChart("Test N° " + select); 
    	myXYSeries rk4 = new myXYSeries("Runge Kutta");
		myXYSeries euler = new myXYSeries("Euler Method");
		World3D world = new World3D(g, player);
		while (n-- != 0) {
			world.updateModel(h,tollerance);
			if (debug) {
				System.out.println("Euler  ->" + world.player.get("Euler").toString());
				System.out.println("RK4    -> " + world.player.get("RK4").toString());
				System.out.println("-----");
			}
			rk4.add(world.player.get("RK4").getPos());
			euler.add(world.player.get("Euler").getPos());
		}
        //Create the chart
        chart.addSeries(rk4);
        chart.addSeries(euler);
        chart.drawChart();
        chart.setSize(1024, 600);
        System.gc();
        RefineryUtilities.centerFrameOnScreen(chart);
        chart.setVisible(true);
        chart.setFocusable(true);
    }
    
    public static void normalSetup(float h, int n, double tollerance, int select) {
    	LineChart chart = new LineChart("Test N° " + select);
    	
    	
    	myXYSeries exact = new myXYSeries("Exact Solution");
    	myXYSeries rk4 = new myXYSeries("Runge Kutta");
    	myXYSeries euler = new myXYSeries("Euler Method");
    	myXYSeries adap_rk4 = new myXYSeries("Adaptive RK");
    	
    	exact.add(initSol);
    	rk4.add(initSol);
    	euler.add(initSol);
    	adap_rk4.add(initSol);
    	/*exact.add(new Vector2D(0,0));
    	euler.add(new Vector2D(0,0));
    	rk4.add(new Vector2D(0,0));
    	double ye = 0;
    	double yr = 0;*/
		World2D world = new World2D(f, initSol.copyVect(), exactf);
		while (n-- != 0) {
			world.updateModel(h,tollerance);
			if (debug) {
				System.out.println("Euler       -> " + world.methods.get("Euler").toString());
				System.out.println("RK4         -> " + world.methods.get("RK4").toString());
				System.out.println("Adaptive RK -> " + world.methods.get("AdapRK4").toString());
				System.out.println("Exact       -> " + world.methods.get("Exact").toString());
				System.out.println("-----");
			}
			exact.add(world.methods.get("Exact"));
			rk4.add(world.methods.get("RK4"));
			adap_rk4.add(world.methods.get("AdapRK4"));
			euler.add(world.methods.get("Euler"));
			/*ye += Math.abs(world.methods.get("AdapRK4").getY()-world.methods.get("Euler").getY());
			euler.add(new Vector2D(world.methods.get("Euler").getX(),ye));
			yr += Math.abs(world.methods.get("AdapRK4").getY()-world.methods.get("RK4").getY());
			rk4.add(new Vector2D(world.methods.get("Euler").getX(),yr));*/
			
		}
    	
        //Create the chart
        chart.addSeries(exact);
        chart.addSeries(rk4);
        chart.addSeries(euler);
        chart.addSeries(adap_rk4);
        System.gc();
        chart.drawChart();
        chart.setSize(1024, 600);
        RefineryUtilities.centerFrameOnScreen(chart);
        chart.setVisible(true);
        chart.setFocusable(true);
    }
    
    public static void select_test(int a) {
    	Entity npc;
    	switch (a) {
    	case 0:
        	f = new test0.f_test();
    		exactf = new test0.exactf();
    		initSol = new Vector2D(0,5);
    		break;
    	case 1:
    		f = new test1.f_test();
    		exactf = new test1.exactf();
    		initSol = new Vector2D(0,6);
    		break;
    	case 2:
    		f = new test2.f_test();
    		exactf = new test2.exactf();
    		initSol = new Vector2D(2,5);
    		break;
    	case 3:
    		f = new test3.f_test();
    		exactf = new test3.exactf();
    		initSol = new Vector2D(1,2);
    		break;
    	case 4:
    		f = new test4.f_test();
    		exactf = new test4.exactf();
    		initSol = new Vector2D(0,0);
    		break;
    	case 5:
    		f = new test5.f_test();
    		exactf = new test5.exactf();
    		initSol = new Vector2D(0,5);
    		break;
    	case 10:
    		player = new Entity();
    		player.setPos(new Vector2D(0, 363104*Math.pow(10,3)));
    		player.setVel(new Vector2D(1082, 0));
    		
    		npc = new Entity();
    		npc.setPos(new Vector2D(0,0));
    		npc.setMass(5.974*Math.pow(10,24));
    		
    		g = new test10.f_test(npc);
    		break;
    	case 11:
    		player = new Entity();
    		player.setPos(new Vector2D(0, 363104*Math.pow(10,3)));
    		player.setVel(new Vector2D(1082, 0));
    		
    		npc = new Entity();
    		npc.setPos(new Vector2D(0,0));
    		npc.setMass(5.974*Math.pow(10,24));
    		
    		g = new test10.f_test(npc);
    		break;
		default:
			System.out.println("Usage: java -jar Simulation.jar <timestep> <cycle> <tollerance> <test>\nFor the list of the test see README" );
			System.exit(5);
			break;
    	}    		
    }
}