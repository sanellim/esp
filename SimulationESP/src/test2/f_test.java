package test2;
import physics.DerivableFunctionScalar;


public class f_test implements DerivableFunctionScalar {
	@Override
	public double f(double x, double y) {
		return x-y;
	}

}
