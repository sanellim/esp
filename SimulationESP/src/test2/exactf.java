package test2;
import physics.DerivableFunctionScalar;


public class exactf implements DerivableFunctionScalar{

	@Override
	public double f(double x, double y) {
		return 4*Math.pow(Math.E, 2-x)+x-1;
	}

	
}
