package test0;
import physics.DerivableFunctionScalar;


public class f_test implements DerivableFunctionScalar {
	@Override
	public double f(double x, double y) {
		return 3*Math.pow(Math.E, -x) - 0.4 * y;
	}

}
