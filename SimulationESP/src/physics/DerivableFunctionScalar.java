package physics;

public interface DerivableFunctionScalar {
	public double f(double x,double y); 
}
