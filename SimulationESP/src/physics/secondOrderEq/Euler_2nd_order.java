package physics.secondOrderEq;

import physics.DerivableFunctionVector;
import general.Entity;
import general.Vector2D;

public class Euler_2nd_order {
	public static Entity runMethod(Entity player, DerivableFunctionVector f, float h) {

		Vector2D acc= f.f(player.getPos());
		player.getPos().setX(player.getPos().getX() + player.getVel().getX()*h);
		player.getPos().setY(player.getPos().getY() + player.getVel().getY()*h);

		player.getVel().setX(player.getVel().getX()+acc.getX()*h);
		player.getVel().setY(player.getVel().getY()+acc.getY()*h);
		
		return player;
	}
}
