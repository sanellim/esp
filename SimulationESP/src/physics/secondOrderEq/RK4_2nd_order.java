package physics.secondOrderEq;

import physics.DerivableFunctionVector;
import general.Entity;
import general.Vector2D;

public class RK4_2nd_order {
		
	public static Entity runMethod(Entity player, DerivableFunctionVector f, float h) {
		
		Vector2D pos_k1 = player.getVel().copyVect();
		pos_k1.mul(h);
		
		Vector2D vel_k1 = f.f(player.getPos()).copyVect();
		vel_k1.mul(h);
		
		Vector2D pos_k2 = player.getVel().copyVect();
		pos_k2.sum(vel_k1.getX()*0.5, vel_k1.getY()*0.5);
		pos_k2.mul(h);
		
		Vector2D vel_k2 = player.getPos().copyVect();
		vel_k2.sum(pos_k1.getX()*0.5, pos_k1.getY()*0.5);
		vel_k2 = f.f( vel_k2 );
		vel_k2.mul(h);
		
		Vector2D pos_k3 = player.getVel().copyVect();
		pos_k3.sum(vel_k2.getX()*0.5, vel_k2.getY()*0.5);
		pos_k3.mul(h);
		
		Vector2D vel_k3 = player.getPos().copyVect();
		vel_k3.sum(pos_k2.getX()*0.5, pos_k2.getY()*0.5);
		vel_k3 = f.f( vel_k3 );
		vel_k3.mul(h);
		
		Vector2D pos_k4 = player.getVel().copyVect();
		pos_k4.sum(vel_k3.getX(), vel_k3.getY());
		pos_k4.mul(h);
		
		Vector2D vel_k4 = player.getPos().copyVect();
		vel_k4.sum(pos_k3.getX()*0.5, pos_k3.getY()*0.5);
		vel_k4 = f.f( vel_k4 );
		vel_k4.mul(h);
		
		player.getPos().setX(player.getPos().getX() + pos_k1.getX()/6 + 
				pos_k2.getX()/3 + pos_k3.getX()/3 + pos_k4.getX()/6);
		player.getPos().setY(player.getPos().getY() + pos_k1.getY()/6 + 
				pos_k2.getY()/3 + pos_k3.getY()/3 + pos_k4.getY()/6);
		
		player.getVel().setX(player.getVel().getX() + vel_k1.getX()/6 + 
				vel_k2.getX()/3 + vel_k3.getX()/3 + vel_k4.getX()/6);
		player.getVel().setY(player.getVel().getY() + vel_k1.getY()/6 + 
				vel_k2.getY()/3 + vel_k3.getY()/3 + vel_k4.getY()/6);
		
		return player;
	}
}
