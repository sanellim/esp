package physics;

import general.Vector2D;

public interface DerivableFunctionVector {
		public Vector2D f(double x,double y);
		public Vector2D f(Vector2D z);
}
