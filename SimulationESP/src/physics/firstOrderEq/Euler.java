package physics.firstOrderEq;

import physics.DerivableFunctionScalar;
import general.Vector2D;

public class Euler {
	public static double runMethod(Vector2D pos, float h, DerivableFunctionScalar f) {
		return pos.getY() + h*f.f(pos.getX(), pos.getY());
	}
}
