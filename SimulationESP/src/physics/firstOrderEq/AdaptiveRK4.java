package physics.firstOrderEq;

import physics.DerivableFunctionScalar;
import general.Vector2D;

public class AdaptiveRK4 {
	public static int a = 31; //(Math.pow(2, 5)-1)
	public static double runMethod(double x, double y, DerivableFunctionScalar f, float h, double tollerance) {
		float h0_5 = h/2;
		double retv = RK4_1th_order.runMethod(x, y, f, h0_5);
		retv = RK4_1th_order.runMethod(x+h0_5, retv, f, h0_5);
		double tmp = RK4_1th_order.runMethod(x, y, f, h);
		
		double epsilon = Math.abs((retv - tmp)/a);
		if (epsilon > tollerance) {
			tmp = AdaptiveRK4.runMethod(x, y, f, h0_5, tollerance);
			retv = AdaptiveRK4.runMethod(x+h0_5, tmp, f, h0_5, tollerance);
		}
		return retv;
	}
	
	public static double runMethod(Vector2D vec, DerivableFunctionScalar f, float h, double tollerance) {
		return runMethod(vec.getX(), vec.getY(), f, h, tollerance);
		
	}
	

}