package general;

public class Vector2D {
	private double x;
	private double y;
	
	public Vector2D() {
		x = 0;
		y = 0;
	}

	public Vector2D(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public double getX() {
		return this.x;
	}
	
	public void setVec(Vector2D vect)
	{
		this.x = vect.getX();
		this.y = vect.getY();
	}
	
	public void setX(double x) {
		this.x = x;
	}
	
	public void setY(double y) {
		this.y = y;
	}
	
	public double getY() {
		return this.y;
	}
	
	public Vector2D copyVect() {
		return new Vector2D(x, y);
	}
	
	public double magnitude() {
		return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
	}
	
	public void mul(double a) {
		this.x *= a;
		this.y *= a;
	}
	
	public void sum(double s_x, double s_y) {
		this.x += s_x;
		this.y += s_y;
	}

	public void sum(Vector2D a) {
		this.x += a.getX();
		this.y += a.getY();
	}
	@Override
	public String toString() {
		return " " + x + " " + y;
		//return String.format("%1$.2f %1$.2f", (int)this.x, (int)this.y);
	}
}
