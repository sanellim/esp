package general;

import java.util.HashMap;

import physics.DerivableFunctionScalar;
import physics.firstOrderEq.AdaptiveRK4;
import physics.firstOrderEq.Euler;
import physics.firstOrderEq.RK4_1th_order;


public class World2D {
	private HashMap<String, DerivableFunctionScalar> func;
	public Entity player;
	public HashMap<String, Vector2D> methods;
	
	/* Func[0] Funzione da calcolare 
	 * func[1] funzione esatta
	 * vectors[0] euler 
	 * vectors[1] RK4
	 * vectors[2] AdapRK4
	 * vectors[3] Exact
	 */
	public World2D(DerivableFunctionScalar f, Vector2D firstSol, DerivableFunctionScalar exactf) {
		this.func = new HashMap<String, DerivableFunctionScalar>();
		this.methods = new HashMap<String, Vector2D>();
		this.func.put("Function", f);
		this.func.put("Exact", exactf);
		this.methods.put("Euler", firstSol.copyVect());
		this.methods.put("RK4", firstSol.copyVect());
		this.methods.put("AdapRK4", firstSol.copyVect());
		this.methods.put("Exact", firstSol);
	}
	
	public void updateModel(float h, double tollerance) {
		Vector2D eu = methods.get("Euler");
		double y = Euler.runMethod(eu, h, this.func.get("Function"));
		eu.setX(eu.getX()+h);
		eu.setY(y);
		
		Vector2D rk4 = methods.get("RK4");
		y = RK4_1th_order.runMethod(rk4.getX(), rk4.getY(), this.func.get("Function"), h);
		rk4.setX(rk4.getX()+h);
		rk4.setY(y);
		
		Vector2D adrk4 = methods.get("AdapRK4");
		y = AdaptiveRK4.runMethod(adrk4, this.func.get("Function"), h, tollerance);
		adrk4.setX(adrk4.getX()+h);
		adrk4.setY(y);

		Vector2D exact = methods.get("Exact");
		exact.setX(exact.getX() + h);
		exact.setY(func.get("Exact").f(exact.getX(), 0));
	}
}
