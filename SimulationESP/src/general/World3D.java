package general;

import java.util.HashMap;

import physics.DerivableFunctionVector;
import physics.secondOrderEq.Euler_2nd_order;
import physics.secondOrderEq.RK4_2nd_order;


public class World3D {
	
	private DerivableFunctionVector f;
	public HashMap<String, Entity> player;
	
	public World3D(DerivableFunctionVector f, Entity player) {
		this.f = f;
		this.player = new HashMap<String, Entity>();
		this.player.put("RK4", player.copyEntity());
		this.player.put("Euler", player);
	}
	
	public void updateModel(float h, double tollerance) {
		RK4_2nd_order.runMethod(player.get("RK4"), f, h);
		Euler_2nd_order.runMethod(player.get("Euler"), f, h);
	}

}
