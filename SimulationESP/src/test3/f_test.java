package test3;
import physics.DerivableFunctionScalar;


public class f_test implements DerivableFunctionScalar {
	@Override
	public double f(double x, double y) {
		return y/x;
	}

}
