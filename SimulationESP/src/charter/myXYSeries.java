package charter;

import general.Vector2D;

import org.jfree.data.xy.XYSeries;


public class myXYSeries extends XYSeries {

	private static final long serialVersionUID = -5808873768061923274L;

	public myXYSeries(@SuppressWarnings("rawtypes") Comparable key) {
		super(key);
	}
	
	public void add(Vector2D vec) {
		super.add(vec.getX(), vec.getY());
	}

}
