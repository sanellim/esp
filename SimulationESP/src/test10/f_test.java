package test10;
import physics.DerivableFunctionVector;
import function.Gravity;
import general.Entity;
import general.Vector2D;


public class f_test implements DerivableFunctionVector {
	private Entity npc;
	
	public f_test(Entity npc) {
		this.npc = npc;
	}
	
	@Override
	public Vector2D f(Vector2D v) {
		return Gravity.acceleration(v, npc);
	}
	@Override
	public Vector2D f(double x, double y) {
		return Gravity.acceleration(new Vector2D(x,y), npc);
	}

}
