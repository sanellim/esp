package test5;
import physics.DerivableFunctionScalar;


public class exactf implements DerivableFunctionScalar{

	@Override
	public double f(double x, double y) {
		return Math.pow(Math.E,-0.4*x)*(2.85714+2.14286*Math.pow(Math.E, 1.4*x));
	}

	
}
