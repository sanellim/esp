package test5;
import physics.DerivableFunctionScalar;


public class f_test implements DerivableFunctionScalar {
	@Override
	public double f(double x, double y) {
		return -0.4*y+3*Math.pow(Math.E,-x);
	}

}
