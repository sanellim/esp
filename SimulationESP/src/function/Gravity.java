package function;

import general.Entity;
import general.Vector2D;

public class Gravity {
	private static double BIG_G = 6.67384E-11;
	
	public static Vector2D acceleration(Vector2D player, Entity npc) {
		double x = player.getX() - npc.getPos().getX();
		double y = player.getY() - npc.getPos().getY();
		double temp = Math.sqrt(x*x + y*y);
		temp = Math.pow(temp, 3);
		double acceleration = Gravity.BIG_G * npc.getMass();
		acceleration /= temp;
		
		return new Vector2D(acceleration*-1*x, acceleration*-1*y);
	}
}
