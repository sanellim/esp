package Entity;

import General.Vector2D;

public class Entity {
	private Vector2D m_position;
	
	public Entity() {
		setPosition(new Vector2D());
	}

	public Vector2D getPosition() {
		return m_position;
	}

	public void setPosition(Vector2D m_position) {
		this.m_position = m_position;
	}
}
