package Entity;

public class Npc extends Entity {
	private double mass;
	
	public Npc() {
		super();
		setMass(0);
	}

	public double getMass() {
		return mass;
	}

	public void setMass(double mass) {
		this.mass = mass;
	}
}
