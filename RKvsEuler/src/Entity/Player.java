package Entity;

import General.Vector2D;

public class Player extends Entity {
	private Vector2D velocity;
	
	public Player() {
		super();
		setVelocity(new Vector2D());
	}

	public Vector2D getVelocity() {
		return velocity;
	}

	public void setVelocity(Vector2D velocity) {
		this.velocity = velocity;
	}
	
	public Player copyPlayer() {
		Player tmp = new Player();
		tmp.setPosition(this.getPosition().copyVect());
		tmp.setVelocity(this.getVelocity().copyVect());
		return tmp;
	}
	
	@Override
	public String toString() {
		return String.format("%s %d %d %s %d %d", "Player vel:", (int)this.getVelocity().getX(), (int)this.getVelocity().getY(), "pos:", (int)this.getPosition().getX(), (int)this.getPosition().getY());
		
	}
}
