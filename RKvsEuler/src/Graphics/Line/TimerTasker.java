package Graphics.Line;
import java.util.*;

public class TimerTasker extends TimerTask {
	private Game g;
	float x;
	float y;
	
	public TimerTasker(Game g) {
		this.g = g;
		x = 0;
		y = 0;
	}
	
	@Override
	public void run() {
		g.world.updateWorld(1000);
		
		g.addPoint((float)(g.world.getPlayer().getPosition().getX()/(Math.pow(10,6))),
				(float)(g.world.getPlayer().getPosition().getY()/(Math.pow(10,6))));
		
	}
	

}
