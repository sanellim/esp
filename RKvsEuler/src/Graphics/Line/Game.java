package Graphics.Line;

import java.util.Timer;

import Entity.Npc;
import Entity.Player;
import General.Vector2D;
import General.World;

public class Game extends Graph {
	boolean started = false;
	float x = 0;
	float y = 0;
	protected World world;
	private Timer timer = new Timer();
	
	public Game() {
		super();
		
		Player player = new Player();
		player.setPosition(new Vector2D(0, 363104*Math.pow(10,3)));
		player.setVelocity(new Vector2D(1082, 0));
		Npc npc = new Npc();
		npc.setPosition(new Vector2D(0,0));
		npc.setMass(5.974*Math.pow(10,24));
		
		world = new World(player, npc);
		world.setMethod((short)1);
	}
	
	@Override
	public void create() {
		super.create();
		
		TimerTasker tt = new TimerTasker(this);
		timer.schedule(tt, 50, 50);
	}
	
	@Override
	public void dispose() {
		timer.cancel();
	}
	
}