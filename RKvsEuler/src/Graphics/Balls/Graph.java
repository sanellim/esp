package Graphics.Balls;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;

public class Graph implements ApplicationListener {
    static final int MAX_LINES = 2000;
    OrthographicCamera camera;
    private Mesh ballMesh;
    
    Mesh euclideanMesh;
       
    @Override 
    public void create() {
		int width = Gdx.graphics.getWidth();
		int height = Gdx.graphics.getHeight();
		camera = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.position.set(width/2, height/2, 0);
        
        euclideanMesh = new Mesh(true, 4, 4, new VertexAttribute(Usage.Position, 2, "a_pos"));
        euclideanMesh.setVertices(new float[] {
        		-Float.MAX_VALUE,0,
        		Float.MAX_VALUE,0,
        		0,-Float.MAX_VALUE,
        		0,Float.MAX_VALUE
        });
        euclideanMesh.setIndices(new short[] {
        		0,1,2,3
        });
        
        ballMesh = new Mesh( false, 4, 0, new VertexAttribute( Usage.Position, 2, "a_position" ) );
        ballMesh.setVertices( new float[] { -5, -5, 
        									5, -5,
                                             5,  5,
                                            -5,  5 } );
    }
    
    private void handleInput() {
        if(Gdx.input.isKeyPressed(Input.Keys.A)) {
                camera.zoom += 0.02;
        }
        if(Gdx.input.isKeyPressed(Input.Keys.Q)) {
                camera.zoom -= 0.02;
        }
        if(Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
                if (camera.position.x > 0)
                        camera.translate(-3, 0, 0);
        }
        if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
                if (camera.position.x < 1024)
                        camera.translate(3, 0, 0);
        }
        if(Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
                if (camera.position.y > 0)
                        camera.translate(0, -3, 0);
        }
        if(Gdx.input.isKeyPressed(Input.Keys.UP)) {
                if (camera.position.y < 1024)
                        camera.translate(0, 3, 0);
        }
    }
    
    
    @Override 
    public void render() {
		handleInput();
        // clear screen
        Gdx.gl.glClearColor(0.5f, 0.5f, 0.5f, 0.5f);
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        
        // update the camera (not necessary as we don't change its properties)
        camera.update();
        camera.apply(Gdx.gl10);
        // render the lines so far, but only if we have at least 2
        
        ballMesh.render(GL10.GL_TRIANGLE_FAN);
        euclideanMesh.render(GL10.GL_LINES);
        
    }
    
    public void addPoint(float x, float y) {
    	ballMesh.setVertices(new float[] {
    			x-2, y-2,
    			x+2, y-2,
    			x+2, y+2,
    			x-2, y+2
    			
    	});
    }

	@Override
	public void dispose() {
		
	}

	@Override
	public void pause() { }

	@Override
	public void resize(int arg0, int arg1) { }

	@Override
	public void resume() { }
}