package General;

public class Vector2D {
	private double x;
	private double y;
	
	public Vector2D() {
		x = 0;
		y = 0;
	}
	
	public double getX() {
		return this.x;
	}
	
	public void setX(double x) {
		this.x = x;
	}
	
	public void setY(double y) {
		this.y = y;
	}
	
	public double getY() {
		return this.y;
	}
	
	public Vector2D(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public Vector2D copyVect() {
		return new Vector2D(x, y);
	}
	
	public double magnitude() {
		return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
	}
	
	@Override
	public String toString() {
		return String.format("%d %d", (int)this.x, (int)this.y);
	}
}
