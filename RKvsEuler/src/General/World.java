package General;

import Entity.Npc;
import Entity.Player;
import Physics.Euler;
import Physics.RK4_2nd_order;

public class World {
	private Player player;
	private Npc npc;
	private short method;
	
	public World() {
		setPlayer(new Player());
		setNpc(new Npc());
		method = 0;
	}
	
	public World(Player player, Npc npc) {
		this();
		this.player = player;
		this.npc = npc;
	}

	public Npc getNpc() {
		return npc;
	}

	public void setNpc(Npc npc) {
		this.npc = npc;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public void setMethod(short m) {
		this.method = m;
	}
	
	public void updateWorld(float t) {
		switch (method){
			case 0: player = Euler.runMethod(player, npc, t); break;
			case 1: player = RK4_2nd_order.runMethod(player, npc, t);
		}
	}
}
