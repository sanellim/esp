package Physics;

import Entity.Npc;
import General.Vector2D;

public class Physics {
	private static double BIG_G = 6.67384E-11;
	
	public static Vector2D acceleration(Vector2D player, Npc npc) {
		double x = player.getX() - npc.getPosition().getX();
		double y = player.getY() - npc.getPosition().getY();
		double temp = Math.sqrt(x*x + y*y);
		temp = Math.pow(temp, 3);
		double acceleration = Physics.BIG_G * npc.getMass();
		acceleration /= temp;
		
		return new Vector2D(acceleration*-1*x, acceleration*-1*y);
	}
}
