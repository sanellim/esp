package Physics;

import Entity.Npc;
import Entity.Player;
import General.Vector2D;

public class RK4_2nd_order {
	public static Player runMethod(Player player, Npc npc, float t) {
				
		Vector2D pos_k1 = new Vector2D();
		pos_k1.setX(player.getVelocity().getX()*t);
		pos_k1.setY(player.getVelocity().getY()*t);
		
		Vector2D acc = Physics.acceleration(player.getPosition(), npc);
		Vector2D vel_k1 = new Vector2D();
		vel_k1.setX(acc.getX()*t);
		vel_k1.setY(acc.getY()*t);
		
		Vector2D pos_k2 = new Vector2D();
		pos_k2.setX((player.getVelocity().getX() + 0.5*vel_k1.getX())*t);
		pos_k2.setY((player.getVelocity().getY() + 0.5*vel_k1.getY())*t);
		
		Vector2D temp = pos_k1.copyVect();
		temp.setX(player.getPosition().getX()+0.5*temp.getX());
		temp.setY(player.getPosition().getY()+0.5*temp.getY());
		Vector2D acctmp = Physics.acceleration(temp, npc);
		Vector2D vel_k2 = new Vector2D();
		vel_k2.setX(acctmp.getX()*t);
		vel_k2.setY(acctmp.getY()*t);
		
		Vector2D pos_k3 = new Vector2D();
		pos_k3.setX((player.getVelocity().getX() + 0.5*vel_k2.getX())*t);
		pos_k3.setY((player.getVelocity().getY() + 0.5*vel_k2.getY())*t);
		
		temp = pos_k2.copyVect();
		temp.setX(player.getPosition().getX()+0.5*temp.getX());
		temp.setY(player.getPosition().getY()+0.5*temp.getY());
		acctmp = Physics.acceleration(temp, npc);
		Vector2D vel_k3 = new Vector2D();
		vel_k3.setX(acctmp.getX()*t);
		vel_k3.setY(acctmp.getY()*t);
		
		Vector2D pos_k4 = new Vector2D();
		pos_k4.setX((player.getVelocity().getX() + vel_k3.getX())*t);
		pos_k4.setY((player.getVelocity().getY() + vel_k3.getY())*t);
		
		temp = pos_k3.copyVect();
		temp.setX(player.getPosition().getX()+0.5*temp.getX());
		temp.setY(player.getPosition().getY()+0.5*temp.getY());
		acctmp = Physics.acceleration(temp, npc);
		Vector2D vel_k4 = new Vector2D();
		vel_k4.setX(acctmp.getX()*t);
		vel_k4.setY(acctmp.getY()*t);
		
		player.getPosition().setX(player.getPosition().getX() + pos_k1.getX()/6 + 
				pos_k2.getX()/3 + pos_k3.getX()/3 + pos_k4.getX()/6);
		player.getPosition().setY(player.getPosition().getY() + pos_k1.getY()/6 + 
				pos_k2.getY()/3 + pos_k3.getY()/3 + pos_k4.getY()/6);
		
		player.getVelocity().setX(player.getVelocity().getX() + vel_k1.getX()/6 + 
				vel_k2.getX()/3 + vel_k3.getX()/3 + vel_k4.getX()/6);
		player.getVelocity().setY(player.getVelocity().getY() + vel_k1.getY()/6 + 
				vel_k2.getY()/3 + vel_k3.getY()/3 + vel_k4.getY()/6);
		
		System.gc();
		return player;
	}
}
