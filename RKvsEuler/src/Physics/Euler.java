package Physics;

import Entity.Npc;
import Entity.Player;
import General.Vector2D;

public class Euler {
	public static Player runMethod(Player player, Npc npc, float t) {
		Player temp = player.copyPlayer();
		Vector2D acc = Physics.acceleration(player.getPosition(), npc);
		
		Vector2D pos = new Vector2D();
		pos.setX(player.getPosition().getX() + player.getVelocity().getX()*t);
		pos.setY(player.getPosition().getY() + player.getVelocity().getY()*t);
		
		Vector2D vel = new Vector2D();
		vel.setX(player.getVelocity().getX() + acc.getX()*t);
		vel.setY(player.getVelocity().getY() + acc.getY()*t);
		
		temp.setPosition(pos);
		temp.setVelocity(vel);
		//System.out.println(temp.toString());
		return temp;
	}
}
