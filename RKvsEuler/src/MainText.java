import Entity.Npc;
import Entity.Player;
import General.Vector2D;
import General.World;

public class MainText {

	private static float t = 100;
	
	public static void Run(String[] args) {
		if (args.length >= 1)
			t = Float.parseFloat(args[0]);
		System.out.println("DT setted to " + t);
		Player player = new Player();
		player.setPosition(new Vector2D(0, 363104*Math.pow(10,3)));
		player.setVelocity(new Vector2D(1082, 0));
		
		Npc npc = new Npc();
		npc.setPosition(new Vector2D(0,0));
		npc.setMass(5.974*Math.pow(10,24));
		
		World world = new World();
		world.setPlayer(player);
		world.setNpc(npc);
		
		long many=0;
		short sgny = +1;
		short sgnx = +1;
		int x = 0;
		boolean y = true;
		while (y) {
			world.updateWorld(t);
			many++;
			if (world.getPlayer().getPosition().getX()*sgnx < 0) {
				sgnx *= -1;
				System.out.println("Euler -> " + world.getPlayer().toString() + " Many - " + many);
				x++;
				if (x==2) y = false;
			}
			if (world.getPlayer().getPosition().getY()*sgny < 0) {
				System.out.println("Euler -> " + world.getPlayer().toString() + " Many - " + many);
				sgny *= -1;
			}
		}
		
		player.setPosition(new Vector2D(0, 363104*Math.pow(10,3)));
		player.setVelocity(new Vector2D(1082, 0));
		world.setPlayer(player);
		world.setMethod((short)1);
		
		System.out.println("");
		many=0;
		sgny = +1;
		sgnx = +1;
		x = 0;
		y = true;
		
		while (y) {
			world.updateWorld(t);
			many++;
			//if (many%500 == 0)System.out.println("RK4 -> " + world.getPlayer().toString() + " Many - " + many);
			if (world.getPlayer().getPosition().getX()*sgnx < 0) {
				sgnx *= -1;
				System.out.println("RK4 -> " + world.getPlayer().toString() + " Many - " + many);
				x++;
				if (x==2) y = false;
			}
			if (world.getPlayer().getPosition().getY()*sgny < 0) {
				System.out.println("RK4 -> " + world.getPlayer().toString() + " Many - " + many);
				sgny *= -1;
			}
			
		}
	}

}
