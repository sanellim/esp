package Test3;
import Physics.DerivableFunction;


public class f_test implements DerivableFunction {
	@Override
	public double f(double x, double y) {
		return y/x;
	}

}
