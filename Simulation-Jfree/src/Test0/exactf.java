package Test0;
import Physics.DerivableFunction;


public class exactf implements DerivableFunction{

	@Override
	public double f(double x, double y) {
		return 10*Math.pow(Math.E, -0.4*x) - 5*Math.pow(Math.E, -x);
	}

	
}
