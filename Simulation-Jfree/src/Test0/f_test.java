package Test0;
import Physics.DerivableFunction;


public class f_test implements DerivableFunction {
	@Override
	public double f(double x, double y) {
		return 3*Math.pow(Math.E, -x) - 0.4 * y;
	}

}
