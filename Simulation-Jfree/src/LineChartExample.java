import org.jfree.data.xy.XYSeries;
import org.jfree.ui.RefineryUtilities;

import Charter.DotChart;
import Charter.MyChart;
import General.Entity;
import General.Vector2D;
import General.World;
import Physics.DerivableFunction;
import Physics.DerivableFunctionV;

/**
 * An example for line chart.
 */
public class LineChartExample {

	private static XYSeries exact = new XYSeries("Exact Solution");
	private static XYSeries rk4 = new XYSeries("Runge Kutta");
	private static XYSeries adap_rk4 = new XYSeries("Adaptive RK");
	private static XYSeries euler = new XYSeries("Euler Method");
	private static XYSeries rk4_2 = new XYSeries("RK2 Method");
	private static XYSeries eu_2 = new XYSeries("EU2 Method");
	private static boolean debug = false;
	private static DerivableFunction f;
	private static DerivableFunctionV g;
	private static Entity npc;
	private static Entity player;
	private static DerivableFunction exactf;
	private static Vector2D initSol;
	
    public static void main(String[] args) {
    	float h = (float) 100;
    	int n = 40000;
    	int select = 4;
    	double tollerance = 1;
    	if (args.length >= 1) h = Float.parseFloat(args[0]); 
    	if (args.length >= 2) n = Integer.parseInt(args[1]);
    	if (args.length >= 3) tollerance = Double.parseDouble(args[2]);
    	if (args.length == 4) select = Integer.parseInt(args[3]);
    	select_test(select);
    	System.out.println("Test " + select + " started with h = " + h + " Cycle = " + n + " Tollerance = " + tollerance);
    	
    	if (select < 4) {
    		normalSetup(h, n, tollerance, select);
    	}
    	else specialSetup(h,n,tollerance,select);
    		
    }
    
    public static void specialSetup(float h, int n, double tollerance, int select) {
    	DotChart chart = new DotChart("Test N� " + select); 
    	
		World world = new World(g, player);
		while (n-- != 0) {
			world.updateModel(h,tollerance);
			if (debug) {
				System.out.println("RK4 Position -> " + world.player.getPos().toString());
				System.out.println("RK4 Velocity -> " + world.player.getVel().toString());
				System.out.println("EU Position -> " + world.playerE.getPos().toString());
				System.out.println("EU Velocity -> " + world.playerE.getVel().toString());
				System.out.println("-----");
			}
			rk4_2.add(world.player.getPos().getX(), world.player.getPos().getY());
			eu_2.add(world.playerE.getPos().getX(), world.playerE.getPos().getY());
			
		}
        //Create the chart
        chart.addSeries(rk4_2);
        chart.addSeries(eu_2);
        chart.drawChart();
        chart.setSize(1024, 600);
        RefineryUtilities.centerFrameOnScreen(chart);
        chart.setVisible(true);
        chart.setFocusable(true);
    }
    
    public static void normalSetup(float h, int n, double tollerance, int select) {
    	MyChart chart = new MyChart("Test N� " + select); 
    	exact.add(initSol.getX(), initSol.getY());
    	rk4.add(initSol.getX(), initSol.getY());
    	euler.add(initSol.getX(), initSol.getY());
    	adap_rk4.add(initSol.getX(), initSol.getY());
		World world = new World(f, initSol.copyVect(), exactf);
		while (n-- != 0) {
			world.updateModel(h,tollerance);
			if (debug) {
				System.out.println("Euler       -> " + world.actual_f.toString());
				System.out.println("RK4         -> " + world.actual_g.toString());
				System.out.println("Adaptive RK -> " + world.actual_q.toString());
				System.out.println("Exact       -> " + world.exact_v.toString());
				System.out.println("-----");
			}
			exact.add(world.exact_v.getX(), world.exact_v.getY());
			rk4.add(world.actual_g.getX(), world.actual_g.getY());
			adap_rk4.add(world.actual_q.getX(), world.actual_q.getY());
			euler.add(world.actual_f.getX(), world.actual_f.getY());
		}
    	
        //Create the chart
        chart.addSeries(exact);
        //chart.addSeries(rk4);
        //chart.addSeries(euler);
        chart.addSeries(adap_rk4);
        chart.drawChart();
        chart.setSize(1024, 600);
        RefineryUtilities.centerFrameOnScreen(chart);
        chart.setVisible(true);
        chart.setFocusable(true);
    }
    
    public static void select_test(int a) {
    	switch (a) {
    	case 0:
        	f = new Test0.f_test();
    		exactf = new Test0.exactf();
    		initSol = new Vector2D(0,5);
    		break;
    	case 1:
    		f = new Test1.f_test();
    		exactf = new Test1.exactf();
    		initSol = new Vector2D(0,6);
    		break;
    	case 2:
    		f = new Test2.f_test();
    		exactf = new Test2.exactf();
    		initSol = new Vector2D(2,5);
    		break;
    	case 3:
    		f = new Test3.f_test();
    		exactf = new Test3.exactf();
    		initSol = new Vector2D(1,2);
    		break;
    	case 4:
    		player = new Entity();
    		player.setPos(new Vector2D(0, 363104*Math.pow(10,3)));
    		player.setVel(new Vector2D(1082, 0));
    		
    		npc = new Entity();
    		npc.setPos(new Vector2D(0,0));
    		npc.setMass(5.974*Math.pow(10,24));
    		initSol = null;
    		
    		g = new Test4.f_test(npc);
    		break;
    	case 5:
    		player = new Entity();
    		player.setPos(new Vector2D(0, 363104*Math.pow(10,3)));
    		player.setVel(new Vector2D(1082, 0));
    		
    		npc = new Entity();
    		npc.setPos(new Vector2D(0,0));
    		npc.setMass(5.974*Math.pow(10,24));
    		initSol = null;
    		
    		g = new Test4.f_test(npc);
    		break;
    	}    		
    
    	
    }
}