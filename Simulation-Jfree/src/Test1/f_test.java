package Test1;
import Physics.DerivableFunction;


public class f_test implements DerivableFunction {
	@Override
	public double f(double x, double y) {
		return 26*Math.cos(x)-5*y;
	}

}
