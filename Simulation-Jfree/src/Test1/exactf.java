package Test1;
import Physics.DerivableFunction;


public class exactf implements DerivableFunction{

	@Override
	public double f(double x, double y) {
		return Math.pow(Math.E, -5*x)+Math.sin(x)+5*Math.cos(x);
	}

	
}
