package General;

import Physics.AdaptiveRK4;
import Physics.DerivableFunction;
import Physics.DerivableFunctionV;
import Physics.FirstOrderEq.Euler;
import Physics.FirstOrderEq.RK4_1th_order;
import Physics.SecondOrderEq.Euler_2nd_order;
import Physics.SecondOrderEq.RK4_2nd_order;

public class World {
	private DerivableFunction f;
	private DerivableFunction exactf;
	private DerivableFunctionV g;
	public Entity player;
	public Entity playerE;
	public Vector2D actual_f;
	public Vector2D actual_g;
	public Vector2D actual_q;
	public Vector2D exact_v;
	
	public World(DerivableFunction f, Vector2D firstSol, DerivableFunction exactf) {
		this.f = f;
		this.actual_f = firstSol;
		this.actual_g = firstSol.copyVect();
		this.actual_q = firstSol.copyVect();
		this.exact_v = firstSol.copyVect();
		this.exactf = exactf;
	}
	
	public World(DerivableFunctionV f, Entity player) {
		this.f = null;
		this.exact_v = null;
		this.g = f;
		this.player = player;
		this.playerE = player.copyEntity();
	}
	
	public void updateModel(float h, double tollerance) {
		if (this.f != null) updateModelN(h, tollerance);
		else updateModelG(h, tollerance);
	}
	
	public void updateModelG(float h, double tollerance) {
		RK4_2nd_order.runMethod(player, g, h);
		Euler_2nd_order.runMethod(playerE, g, h);
	}
	
	public void updateModelN(float h, double tollerance) {
		
		double yf = Euler.runMethod(actual_f.getX(), actual_f.getY(), h, f);
		actual_f.setX(actual_f.getX() + h);
		actual_f.setY(yf);
		
		double yq = AdaptiveRK4.runMethod(actual_q.getX(), actual_q.getY(), f, h, tollerance);
		actual_q.setX(actual_q.getX()+h);
		actual_q.setY(yq);
		
		double yg = RK4_1th_order.runMethod(actual_g.getX(), actual_g.getY(), f, h);
		actual_g.setX(actual_g.getX() + h);
		actual_g.setY(yg);
		
		exact_v.setX(exact_v.getX() + h);
		exact_v.setY(exactf.f(exact_v.getX(), 0));
		
	}
}
