package General;

public class Entity {
	private Vector2D m_pos;
	private Vector2D m_vel;
	private double m_mass;
	
	public Entity() {
		m_pos = null;
		m_vel = null;
		m_mass = 0;
	}
	
	public void setPos(Vector2D pos) {
		m_pos = pos;
	}

	public void setVel(Vector2D vel) {
		m_vel = vel;
	}
	
	public Vector2D getVel() {
		return m_vel;
	}
	
	public Vector2D getPos() {
		return m_pos;
	}
	
	public void setMass(double mass) {
		m_mass = mass;
	}
	
	public double getMass() {
		return m_mass;
	}
	
	public Entity copyEntity() {
		Entity retv = new Entity();
		retv.setPos(m_pos.copyVect());
		retv.setVel(m_vel.copyVect());
		return retv;
	}
}
