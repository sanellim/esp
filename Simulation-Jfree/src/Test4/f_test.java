package Test4;
import General.Entity;
import General.Vector2D;
import Physics.DerivableFunctionV;
import Physics.Physics;


public class f_test implements DerivableFunctionV {
	private Entity npc;
	
	public f_test() {
		npc = new Entity();
		npc.setPos(new Vector2D(0,0));
		npc.setVel(new Vector2D(0,0));
	}
	
	public f_test(Entity npc) {
		this.npc = npc;
	}
	
	@Override
	public Vector2D f(Vector2D v) {
		return Physics.acceleration(v, npc);
	}
	@Override
	public Vector2D f(double x, double y) {
		// TODO Auto-generated method stub
		return null;
	}

}
