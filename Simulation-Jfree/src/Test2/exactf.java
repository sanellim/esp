package Test2;
import Physics.DerivableFunction;


public class exactf implements DerivableFunction{

	@Override
	public double f(double x, double y) {
		return 4*Math.pow(Math.E, 2-x)+x-1;
	}

	
}
