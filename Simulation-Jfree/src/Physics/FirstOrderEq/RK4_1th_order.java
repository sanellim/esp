package Physics.FirstOrderEq;

import Physics.DerivableFunction;

public class RK4_1th_order {
	public static double runMethod(double x, double y, DerivableFunction f, float h) {
		double h0_5 = h*0.5;
		double k1 = f.f(x, y);
		double k2 = f.f(x + h0_5, y + h0_5*k1);
		double k3 = f.f(x+h0_5, y+h0_5*k2);
		double k4 = f.f(x+h, y+k3*h);
		return y + ((1*h)/6)*(k1 + 2*k2 + 2*k3 + k4);
	}
	
	public static double singleStep(double x, double y, DerivableFunction f, float h) {
		double h0_5 = h*0.5;
		double k1 = f.f(x, y);
		double k2 = f.f(x + h0_5, y + h0_5*k1);
		double k3 = f.f(x+h0_5, y+h0_5*k2);
		double k4 = f.f(x+h, y+k3*h);
		return ((1*h)/6)*(k1 + 2*k2 + 2*k3 + k4);
	}
}
