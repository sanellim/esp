package Physics.FirstOrderEq;

import Physics.DerivableFunction;

public class Euler {
	public static double runMethod(double x, double y, float h, DerivableFunction f) {
		return y + h*f.f(x, y);
	}
}
