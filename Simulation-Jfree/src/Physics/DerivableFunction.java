package Physics;

public interface DerivableFunction {
	public double f(double x,double y);
}
