package Physics;

import General.Vector2D;

public interface DerivableFunctionV {
		public Vector2D f(double x,double y);
		public Vector2D f(Vector2D z);
}
