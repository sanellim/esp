#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define BIG_G 6.67384E-11

float DT=100;

typedef struct vec2d {
	double x;
	double y;
} t_vec2d;

typedef struct entity {
	t_vec2d *pos;
	t_vec2d *vel;
	double mass;
} t_entity;

t_vec2d *f_Acceleration(t_entity player, t_entity npc) {
	double x = player.pos->x - npc.pos->x;
	double y = player.pos->y - npc.pos->y;
/*	if (x<0) x *= -1;
	if (y<0) y *= -1;*/
	double tmp = sqrt(x*x + y*y);
	tmp = pow(tmp,3);
	double acceleration = BIG_G * npc.mass;
	acceleration /= tmp;
	t_vec2d *retv = malloc(sizeof(t_vec2d));
	retv->x = acceleration * -1 * (x);
	retv->y = acceleration * -1 * (y);
	return retv;
}

void f_Euler(t_entity player, t_entity npc) {
	t_vec2d *acc = f_Acceleration(player, npc);
	player.pos->x = player.pos->x + player.vel->x*DT;
	player.pos->y = player.pos->y + player.vel->y*DT;

	player.vel->x = player.vel->x + acc->x*DT;
	player.vel->y = player.vel->y + acc->y*DT;
	free(acc);
}

t_entity* f_Rk4(t_entity player, t_entity npc, float t) {
	t_entity *temp = malloc(sizeof(t_entity));
	temp->pos = malloc(sizeof(t_vec2d));
	temp->vel = malloc(sizeof(t_vec2d));
	temp->pos->x = player.pos->x;
	temp->pos->y = player.pos->y;
	temp->vel->x = player.vel->x;
	temp->vel->y = player.vel->y;

	t_vec2d *acc = f_Acceleration(player, npc);

	t_vec2d pos_k1;
	pos_k1.x = player.vel->x * t;
	pos_k1.y = player.vel->y * t;

	t_vec2d vel_k1;
	vel_k1.x = acc->x*t;
	vel_k1.y = acc->y*t;

	t_vec2d pos_k2;
	pos_k2.x = (player.vel->x + 0.5*vel_k1.x)*t;
	pos_k2.y = (player.vel->y + 0.5*vel_k1.y)*t;

	temp->pos->x = player.pos->x + 0.5*pos_k1.x;
	temp->pos->y = player.pos->y + 0.5*pos_k1.y;
	t_vec2d *acctmp = f_Acceleration(*temp, npc);
	t_vec2d vel_k2;
	vel_k2.x = acctmp->x*t;
	vel_k2.y = acctmp->y*t;

	t_vec2d pos_k3;
	pos_k3.x = (player.vel->x + 0.5*vel_k2.x)*t;
	pos_k3.y = (player.vel->y + 0.5*vel_k2.y)*t;

	free(acctmp);
	temp->pos->x = player.pos->x + 0.5*pos_k2.x;
	temp->pos->y = player.pos->y + 0.5*pos_k2.y;
	acctmp = f_Acceleration(*temp, npc);
	t_vec2d vel_k3;
	vel_k3.x = acctmp->x*t;
	vel_k3.y = acctmp->y*t;

	t_vec2d pos_k4;
	pos_k4.x = (player.vel->x + vel_k3.x)*t;
	pos_k4.y = (player.vel->y + vel_k3.y)*t;

	free(acctmp);
	temp->pos->x = player.pos->x + /*0.5**/pos_k3.x;
	temp->pos->y = player.pos->y + /*0.5**/pos_k3.y;
	acctmp = f_Acceleration(*temp, npc);
	t_vec2d vel_k4;
	vel_k4.x = acctmp->x*t;
	vel_k4.y = acctmp->y*t;

	temp->pos->x = player.pos->x + pos_k1.x/6 + pos_k2.x/3 + pos_k3.x/3 + pos_k4.x/6;
	temp->pos->y = player.pos->y + pos_k1.y/6 + pos_k2.y/3 + pos_k3.y/3 + pos_k4.y/6;

	temp->vel->x = player.vel->x + vel_k1.x/6 + vel_k2.x/3 + vel_k3.x/3 + vel_k4.x/6;
	temp->vel->y = player.vel->y + vel_k1.y/6 + vel_k2.y/3 + vel_k3.y/3 + vel_k4.y/6;

	free(player.vel);
	free(player.pos);
	free(acc);
	free(acctmp);
	return temp;
}



int main(int argc, char *argv[]) {
	if (argc < 2) DT = (float)100;
	else DT = atof(argv[1]);
	printf("DT setted to %f\n", DT);
	t_entity *player = malloc(sizeof(t_entity));
	player->pos = malloc(sizeof(t_vec2d));
	player->vel = malloc(sizeof(t_vec2d));
	player->pos->x = 0;
	player->pos->y = 363104*pow(10,3);
	player->vel->x = 1082;
	player->vel->y = 0;

	t_entity npc;
	npc.pos = malloc(sizeof(t_vec2d));
	npc.pos->x = 0;
	npc.pos->y = 0;
	npc.mass = 5.974*pow(10,24);

	long many=0;
	short sgnx = +1;
	short sgny = +1;
	int x=0;
	int y=1;
	while (y) {
		f_Euler(*player, npc);
		many++;
		if (player->pos->y*sgny < 0) {
			sgny *= -1;
			printf("Euler -> Player vel: %5.0f %5.0f pos: %5.0f %5.0f cycle: %ld\n", player->vel->x, player->vel->y, player->pos->x, player->pos->y, many);
		}
		if (player->pos->x*sgnx < 0) {
			sgnx *= -1;
			printf("Euler -> Player vel: %5.0f %5.0f pos: %5.0f %5.0f cycle: %ld\n", player->vel->x, player->vel->y, player->pos->x, player->pos->y, many);
			x++;
			if (x==2) break;
		}

	}

	printf("\n");
	player->pos->x = 0;
	player->pos->y = 363104*pow(10,3);
	player->vel->x = 1082;
	player->vel->y = 0;
	npc.mass = 5.974*pow(10,24);
	many = 0;
	sgnx = +1;
	sgny = +1;
	x=0;

	t_entity *a;
	while (1) {
		a = f_Rk4(*player, npc, DT);
		free(player->vel);
		free(player->pos);
		free(player);
		player = a;
		/*b = f_Rk4(player, npc, 2*DT);*/
		many++;
		if (player->pos->y*sgny < 0) {
			sgny *= -1;
			printf("RK4 -> Player vel: %5.0f %5.0f pos: %5.0f %5.0f cycle: %ld\n", player->vel->x, player->vel->y, player->pos->x, player->pos->y, many);
		}
		if (player->pos->x*sgnx < 0) {
			sgnx *= -1;
			printf("RK4 -> Player vel: %5.0f %5.0f pos: %5.0f %5.0f cycle: %ld\n", player->vel->x, player->vel->y, player->pos->x, player->pos->y, many);
			x++;
			if (x==2) break;
		}

	}
	return 0;
}
