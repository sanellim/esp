\select@language {italian}
\contentsline {section}{\numberline {1}Introduzione}{2}
\contentsline {subsection}{\numberline {1.1}Un esempio: Problema Fisico}{2}
\contentsline {section}{\numberline {2}Analisi Numerica}{2}
\contentsline {subsection}{\numberline {2.1}Problema di Cauchy}{2}
\contentsline {subsection}{\numberline {2.2}Metodi numerici ad un passo}{3}
\contentsline {subsection}{\numberline {2.3}Analisi della convergenza di un metodo numerico ad un passo}{4}
\contentsline {subsection}{\numberline {2.4}Runge Kutta}{6}
\contentsline {subsection}{\numberline {2.5}$RK^{4th}$}{7}
\contentsline {section}{\numberline {3}Effettiva Implementazione}{7}
\contentsline {subsection}{\numberline {3.1}Adattivit\'a dei metodi RK}{9}
\contentsline {section}{\numberline {4}Conclusioni}{10}
